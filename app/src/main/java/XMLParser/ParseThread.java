package XMLParser;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.example.batisto4ka.rssreader.R;

import java.io.IOException;
import java.util.ArrayList;


/**
 * Class for implementing async task to load rss items
 *
 * @author Natalia Makarenko
 */
public class ParseThread extends AsyncTask<String, Integer, ArrayList<PostItem>> {
    /**
     * Broadcast intent action to send loaded data to activity
     */
    public static final String ON_DOWNLOADED_ACTION = "com.example.batisto4ka.app.broadcast_action";
    /**
     * String value to put rss items (PostItem object)in intent
     */
    public static final String ITEMS = "ITEMS";
    /**
     * XML RSSParser object
     */
    private RSSParser parser;
    /**
     * Activity context
     */
    Context context;

    /**
     * Set activity context
     *
     * @param context
     */
    public void setContext(Context context) {
        this.context = context;
    }

    protected void onPreExecute() {
        parser = new RSSParser();
    }

    /**
     * Execute AsyncTask
     *
     * @param url
     * @return
     */
    @Override
    protected ArrayList<PostItem> doInBackground(String... url) {
        try {
            return parser.parse(url[0]);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Send result to activity
     *
     * @param result - loaded rss items
     */
    protected void onPostExecute(ArrayList<PostItem> result) {
        Intent resultIntent = new Intent(ON_DOWNLOADED_ACTION);
        resultIntent.putExtra(ITEMS, result);
        context.sendBroadcast(resultIntent);
    }


}
