package XMLParser;

import android.content.Context;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * CClass that implements rss xml parsing
 *
 * @author Natalie Makarenko
 */
public class RSSParser {
    /**
     * String values for RSS root and elements
     */
    private final String RSS_ITEM = "item";
    private final String RSS_TITLE = "title";
    private final String RSS_DESCRIPTION = "description";
    private final String RSS_LINK = "link";
    private final String RSS_DATE = "pubDate";
    private final String RSS_AUTHOR = "author";
    private final String RSS_IMAGE = "src='http://";

    /**
     * XML parser
     *
     * @return ArrayList<PostItem>
     */
    public ArrayList<PostItem> parse(String address) throws IOException {
        final PostItem currentPost = new PostItem();
        final ArrayList<PostItem> messages = new ArrayList<PostItem>();
        try {
            //open an URL connection make GET to the server and
            //take xml RSS data
            URL url = new URL(address);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = conn.getInputStream();
                //DocumentBuilderFactory, DocumentBuilder are used for
                //xml parsing
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                //using db (Document Builder) parse xml data and assign
                //it to Element
                Document document = db.parse(inputStream);
                Element element = document.getDocumentElement();
                //take rss nodes to NodeList
                NodeList nodeList = element.getElementsByTagName(RSS_ITEM);
                if (nodeList.getLength() > 0) {
                    for (int i = 0; i < nodeList.getLength(); i++) {
                        //take each entry (corresponds to <item></item> tags in
                        //xml data
                        Element entry = (Element) nodeList.item(i);
                        Element title = (Element) entry.getElementsByTagName(RSS_TITLE).item(0);
                        Element description = (Element) entry.getElementsByTagName(RSS_DESCRIPTION).item(0);
                        Element pubDate = (Element) entry.getElementsByTagName(RSS_DATE).item(0);
                        Element link = (Element) entry.getElementsByTagName(RSS_LINK).item(0);
                        Element author = (Element) entry.getElementsByTagName(RSS_AUTHOR).item(0);
                        currentPost.setTitle(title.getFirstChild().getNodeValue());
                        currentPost.setDescription(description.getFirstChild().getNodeValue());
                        currentPost.setDate(pubDate.getFirstChild().getNodeValue());
                        currentPost.setLink(link.getFirstChild().getNodeValue());
                        currentPost.setAuthor(author.getFirstChild().getNodeValue());
                        currentPost.setImageSource(getCharacterDataFromElement(description));
                        messages.add(currentPost.copy());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messages;
    }

    /**
     * Parse string to get image source url
     *
     * @param e - Element object
     * @return - image source url string
     */
    public String getCharacterDataFromElement(Element e) {
        Node child = e.getChildNodes().item(1);
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            String result = cd.getData();
            int start = result.indexOf(RSS_IMAGE) + 5;
            int end = result.indexOf("'", start);
            result = result.substring(start, end);
            return result;
        }
        return "";
    }
}
