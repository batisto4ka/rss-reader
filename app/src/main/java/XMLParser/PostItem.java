package XMLParser;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Class for representing rss items
 *
 * @author Natalie Makarenko
 */
public class PostItem implements Parcelable {
    /*String for data format*/
    static SimpleDateFormat FORMATTER = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss", Locale.ENGLISH);
    /*Post Title*/
    private String title;
    /*Post URL */
    private URL link;
    /*String value of URL*/
    private String linkText;
    /*Post description*/
    private String description;
    /*Post date*/
    private Date date;
    /**
     * Post author
     */
    private String author;
    /**
     * Post image
     */
    private String imageSrc;

    /**
     * Setter for title
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title.trim();
    }

    /**
     * Getter for title
     *
     * @return title
     */
    public String getTitle() {
        return this.title;
    }

    /**
     * Setter for description
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description.trim();
    }

    /**
     * Getter for description
     *
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for link and linkText
     *
     * @param link
     */
    public void setLink(String link) {
        this.linkText = link.trim();
        try {
            this.link = new URL(link);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Getter for link
     *
     * @return
     */
    public URL getLink() {
        return this.link;
    }

    /**
     * Getter for linkText
     *
     * @return
     */
    public String getLinkText() {
        return this.linkText;
    }

    /**
     * Setter for date
     *
     * @param date
     */
    public void setDate(String date) {
        // this.date=date;
        try {
            this.date = FORMATTER.parse(date.trim());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Getter для date
     *
     * @return
     */
    public String getDate() {
        return FORMATTER.format(this.date);
    }

    /**
     * Getter for author field
     *
     * @return
     */
    public String getAuthor() {
        return this.author;
    }

    /**
     * Setter for author field
     *
     * @param author
     */
    public void setAuthor(String author) {
        this.author = author;
    }

    /**
     * Setter for image source ulr
     *
     * @param imageSource
     */
    public void setImageSource(String imageSource) {
        this.imageSrc = imageSource;
    }

    /**
     * Getter for image source url
     *
     * @return
     */
    public String getImageSource() {
        return imageSrc;
    }

    /*Create and return new instance of PostItem filled with current object
    * @return copy
    */
    public PostItem copy() {
        PostItem copy = new PostItem();
        copy.title = title;
        copy.link = link;
        copy.linkText = linkText;
        copy.description = description;
        copy.date = date;
        copy.author = author;
        copy.imageSrc = imageSrc;
        return copy;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Write class object to parcel
     *
     * @param out
     * @param flags
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(title);
        out.writeString(linkText);
        out.writeString(description);
        out.writeString(getDate());
        out.writeString(getAuthor());
        out.writeString(getImageSource());
    }

    public static final Creator<PostItem> CREATOR = new Creator<PostItem>() {
        public PostItem createFromParcel(Parcel source) {
            return new PostItem(source);
        }

        public PostItem[] newArray(int size) {
            return new PostItem[size];
        }
    };

    public PostItem() {
    }

    /**
     * Reed parse object from parcel
     *
     * @param source
     */
    private PostItem(Parcel source) {
        setTitle(source.readString());
        setLink(source.readString());
        setDescription(source.readString());
        setDate(source.readString());
        setAuthor(source.readString());
        setImageSource(source.readString());
    }
}

