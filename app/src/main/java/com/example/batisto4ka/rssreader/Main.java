package com.example.batisto4ka.rssreader;


import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import XMLParser.ParseThread;
import XMLParser.PostItem;


/**
 * Main application activity
 *
 * @author Natalia Makarenko
 */
public class Main extends ActionBarActivity {
    /**
     * Url for parsing
     */
    String url = "http://www.cbc.ca/cmlink/rss-topstories";
    /**
     * String value to save loaded rss items in bundle
     */
    static final String ITEMS = "ITEMS";
    /**
     * String value to save listView instance state in bundle
     */
    static final String LISTVIEW_STATE = "LISTVIEW_STATE";
    /**
     * String value to save progress dialog state in bundle
     */
    static final String LOADING_STATE = "LOADING_STATE";
    /**
     * ListView with loaded rss items
     */
    ListView listView;
    /**
     * ArrayList with loaded items
     *
     * @see XMLParser.PostItem
     */
    ArrayList<PostItem> items;
    /**
     * Reference to current main activity
     */
    Main activity;
    /**
     * Broadcast receiver to get loaded data
     */
    BroadcastReceiver dataLoadedReceiver;
    /**
     * Current progress dialog state
     */
    boolean loadingState;
    /**
     * Progress dialog that shows data loading progress
     */
    ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        activity = this;
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.material_deep_teal_500)));
        getSupportActionBar().setHomeButtonEnabled(true);
        initBroadcastReceiver();
        listView = (ListView) findViewById(R.id.list);
        if (savedInstanceState != null) {//if orientation changed
            items = savedInstanceState.getParcelableArrayList(ITEMS);
            listView.setAdapter(new RSSPostAdapter(this, items));
            listView.onRestoreInstanceState(savedInstanceState.getParcelable(LISTVIEW_STATE));
            loadingState = savedInstanceState.getBoolean(LOADING_STATE);
            if (loadingState)
                loadingDialog = ProgressDialog.show(this, "", getResources().getString(R.string.loading_data), true, false);
        } else {
            loadRss();
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (items != null) {
                    Intent webViewActivity = new Intent(activity, WebViewActivity.class);
                    webViewActivity.putExtra(WebViewActivity.INTENT_POST_URL, items.get(i).getLinkText());
                    activity.startActivity(webViewActivity);
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        if(dataLoadedReceiver==null)
            initBroadcastReceiver();

    }
    @Override
    public void onPause(){
        super.onPause();
        if(dataLoadedReceiver!=null)
            unregisterReceiver(dataLoadedReceiver);
     }
    /**
     * Method that returns network connection state
     *
     * @return - network connection state
     */
    boolean checkNetworkConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnectedOrConnecting());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.update) {
            if (!loadingState)
                loadRss();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method that loads rss items
     */
    void loadRss() {
        if (checkNetworkConnection()) {
            ParseThread parseThread = new ParseThread();
            parseThread.setContext(this);
            parseThread.execute(url);
            loadingState = true;
            loadingDialog = ProgressDialog.show(this, "", getResources().getString(R.string.loading_data), true, false);
        } else
            Toast.makeText(this, getString(R.string.connection_error), Toast.LENGTH_LONG).show();
    }

    /**
     * Method for broadcast receiver initialization
     */
    void initBroadcastReceiver() {
        dataLoadedReceiver = new BroadcastReceiver() {
            /**
             * Receive loaded data
             * @param context
             * @param intent
             */
            public void onReceive(Context context, Intent intent) {
                loadingDialog.dismiss();
                items = intent.getParcelableArrayListExtra(ParseThread.ITEMS);
                if (items!=null)
                    listView.setAdapter(new RSSPostAdapter(activity, items));
                else Toast.makeText(activity, getString(R.string.xml_error), Toast.LENGTH_LONG).show();
                loadingState = false;
            };
        };
        IntentFilter intentFilter = new IntentFilter(ParseThread.ON_DOWNLOADED_ACTION);
        // register BroadcastReceiver
        activity.registerReceiver(dataLoadedReceiver, intentFilter);
    }

    /**
     * Save data and data state to bundle
     *
     * @param savedInstanceState
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        if (items != null)
            savedInstanceState.putParcelableArrayList(ITEMS, items);
        savedInstanceState.putBoolean(LOADING_STATE, loadingState);
        savedInstanceState.putParcelable(LISTVIEW_STATE, listView.onSaveInstanceState());
        super.onSaveInstanceState(savedInstanceState);
    }

}
