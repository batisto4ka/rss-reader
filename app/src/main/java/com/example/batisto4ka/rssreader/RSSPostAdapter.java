package com.example.batisto4ka.rssreader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import XMLParser.PostItem;

/**
 * Custom adapter for listView with loaded rss items
 *
 * @author Natalia Makarenko
 */
public class RSSPostAdapter extends BaseAdapter {
    /**
     * RSS items
     */
    private ArrayList<PostItem> items;
    /**
     * Layout Inflater for custom listview
     */
    LayoutInflater inflater;
    /**
     * Activity context
     */
    Activity context;
    /**
     * Current display image source
     */
    String imageSrc;


    /**
     * Constructor
     *
     * @param context - activity context
     * @param data    - post items
     */
    public RSSPostAdapter(Activity context, ArrayList<PostItem> data) {
        this.context = context;
        items = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Get adapter items count
     *
     * @return adapter items count
     */
    @Override
    public int getCount() {
        if (items != null)
            return items.size();
        return 0;
    }

    /**
     * Get current adapter item
     *
     * @param i - position
     * @return
     */
    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    /**
     * Get current adapter item id
     *
     * @param i - position
     * @return
     */
    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * get current adapter View
     *
     * @param i           - position
     * @param convertView
     * @param parent
     * @return
     * @see com.squareup.picasso.Picasso
     */
    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.listview_item, parent, false);
        TextView postTitle = (TextView) rowView.findViewById(R.id.titleId);
        TextView postDate = (TextView) rowView.findViewById(R.id.pubDateId);
        TextView postAuthor = (TextView) rowView.findViewById(R.id.authorId);
        ImageView postImage = (ImageView) rowView.findViewById(R.id.imageId);
        postTitle.setText(items.get(i).getTitle());
        postDate.setText(items.get(i).getDate());
        postAuthor.setText(items.get(i).getAuthor());
        imageSrc = items.get(i).getImageSource();
        if (imageSrc != null) {
            Picasso.with(context).load(imageSrc).into(postImage);
        }
        return rowView;
    }

}
