package com.example.batisto4ka.rssreader;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

/**
 * Activity that shows selected post in Main activity listView
 *
 * @author Natalia Makarenko
 */
public class WebViewActivity extends ActionBarActivity {
    /*
    String value to put post url to intent
     */
    public static final String INTENT_POST_URL = "INTENT_POST_URL";
    /**
     * WebView object
     */
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_web_view);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.material_deep_teal_500)));
        String url;
        try {
            url = getIntent().getStringExtra(INTENT_POST_URL);
            webView = (WebView) findViewById(R.id.webView);
            webView.getSettings().setJavaScriptEnabled(true); // enable javascript
            webView.loadUrl(url);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.connection_error), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_web_view, menu);
        return true;
    }


}
